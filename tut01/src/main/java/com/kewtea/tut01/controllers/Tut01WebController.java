package com.kewtea.tut01.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("web")
public class Tut01WebController {
	@RequestMapping("web")
	String hello() {
		return "tut01/hello";
	}
}
