package com.kewtea.tut01.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test")
public class Tut01RestController {
	
	@Autowired
	//Tut01Service tut01Service;
	
	
	@RequestMapping("test1")
	String test1() {
		return "Hello";
	}
}
